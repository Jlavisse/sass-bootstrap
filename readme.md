# Quickstart SASS + Bootstrap

## Getting started

Entrez cette commande à la racine du projet:
```bash
npm i
```

## SASS

Entrez cette commande à la racine du projet
```bash
sass --watch scss/main.scss:css/style.css
```

Si vous obtenez cette erreur:

> Error: Invalid CSS after "...lor}: #{$value}": expected "{", was ";"
        on line 4 of node_modules/bootstrap/scss/_root.scss
        from line 11 of node_modules/bootstrap/scss/bootstrap.scss
        from line 1 of scss/main.scss
  Use --trace for backtrace.

... C'est que votre version de SASS n'est pas suffisante

## Installation de la dernière version de SASS

```bash

# Désinstallation des précédentes versions
sudo npm uninstall -g sass
sudo apt update
sudo apt remove sass

# Installation de la nouvelle version
wget https://github.com/sass/dart-sass/releases/download/1.22.3/dart-sass-1.22.3-linux-x64.tar.gz

tar zxvf dart-sass-1.22.3-linux-x64.tar.gz
sudo mv dart-sass /opt/sass
nano ~/.bashrc
```

```bash
# A écrire à la fin du fichier
export PATH="/opt/sass:$PATH"
```
Et enfin: 
```bash
source ~/.bashrc
```

## Remarques

NPM installera aussi les scripts JavaScript jQuery/Popper.JS ainsi que l'intégralité de Bootstrap.  

Ce sont les trois scripts présents à la fin du `<body>` dans le [Getting started de Bootstrap](https://getbootstrap.com/docs/4.3/getting-started/introduction/#starter-template).

On les importe ici en les piochant dans le dossier node_modules!